import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { RouletteService } from '../shared/roulette.service';

@Directive({
  selector: '[appColor]'
})

export class ColorDirective implements OnInit{
  @Input()number!: Number[];
  @Input() set appColor(number: string){
    const num = parseFloat(number);
    console.log(num);
    this.colorClass =  this.rouletteService.getColor(num);
    this.renderer.addClass(this.el.nativeElement, this.colorClass);

  }

  colorClass = 'color';

  constructor(private rouletteService: RouletteService, private el:ElementRef, private renderer: Renderer2){}


  ngOnInit(){
  }


}
