import { EventEmitter } from '@angular/core';

export class RouletteService {
  newNumber = new EventEmitter<Number[]>();
  private numbers: number[] = [];
  private interval: number = 0;


  getNumber(){
    return this.numbers.slice();
  }


  start(){
    this.interval = setInterval((number: number) => {
      this.numbers.push(this.generateNumber());
      this.newNumber.emit(this.numbers);
    }, 1000);

  }

  generateNumber(){
    return Math.floor(Math.random() * 36);
  }

  stop(){
    clearInterval(this.interval);
  }

  reset(){
    this.numbers.splice(this.newNumber.length);
  }

  getColor(number: number){

      if (number >= 1 && number <= 10) {
        return (this.numbers.length % 2 === 0) ? 'black': 'red';
      } else  if (number >= 19 &&  number <= 28){
        return (this.numbers.length % 2 === 0) ? 'black': 'red';
      } else if (number >= 11 && number <= 18){
        return (this.numbers.length % 2 === 0) ? 'red': 'black';
      } else if (number >= 29 && number <= 38) {
        return (this.numbers.length % 2 === 0) ? 'red' : 'black';
      } else if (number === 0){
        return 'zero';
      } else {
        return 'unknown'
      }
  }
}
