import { Component, Input, OnInit } from '@angular/core';
import { RouletteService } from './shared/roulette.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  numbers!: number[];
  @Input() number!: number;
  mode = '';
  inputNumber:number = 0;
  count = 100;


  constructor(private rouletteService: RouletteService){}

  ngOnInit(){
    this.numbers = this.rouletteService.getNumber();
    this.rouletteService.newNumber.subscribe((numbers: number[]) => {
      this.numbers = numbers;

      this.numbers.forEach((item) => {

        if(this.mode && this.rouletteService.getColor(item) === 'red'){
          this.count += this.inputNumber;
        } else if(this.mode && this.rouletteService.getColor(item) === 'black'){
          this.count += this.inputNumber;
        } else if (this.mode && this.rouletteService.getColor(item) === 'zero') {
          this.count +=  this.inputNumber * 35;
        } else {
          this.count -= this.inputNumber;
        }
      });
    });
  }

  OnClick() {
    this.rouletteService.start();
  }

  onStop() {
    this.rouletteService.stop();
  }

  onReset(){
    this.rouletteService.reset();
    this.count= 100;
  }
}
